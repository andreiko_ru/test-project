package funcs

import "github.com/Sirupsen/logrus"

func Download(url string, logger logrus.FieldLogger) {
	logger.Infof("Downloading %s", url)
}
